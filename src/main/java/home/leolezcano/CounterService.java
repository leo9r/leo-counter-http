package home.leolezcano;

import static spark.Spark.*;
import java.util.concurrent.atomic.AtomicInteger;

public class CounterService {
	static AtomicInteger count = new AtomicInteger(0);
    public static void main(String[] args) {
        get("/counter", "application/json", (req, res) -> "{count:" + count + "}");
        post("/increment", "application/json", (req, res) -> "{count:" + count.incrementAndGet() + "}");
    }
}